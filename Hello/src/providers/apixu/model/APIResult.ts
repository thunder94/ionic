import {Location} from "./location";
import {WeatherDetail} from "./weatherDetail";
import {Forecast} from "./forecast";

export interface APIResult {
    location: Location;
    current: WeatherDetail;
    forecast: Forecast;
}