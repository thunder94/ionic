import {ForecastDay} from "./forecastDay";

export interface Forecast {
    forecastday : ForecastDay[];
}