import {Condition} from "./condition";

export interface WeatherDetail {
    last_updated_epoch: string;
    last_updated: string;
    temp_c: number;
    temp_f: number;
    is_day: number;
    condition: Condition;
    wind_mph: number;
    wind_kph: number;
    wind_degree: number;
    wind_dir: string;
    pressure_mb: number;
    pressure_in: number;
    precip_mm: number;
    precip_in: number;
    humidity: number;
    cloud: number;
    feelslike_c: number;
    feelsike_f: number;
    vis_km: number;
    vis_miles: number;
    uv: number;
}
export interface WeatherPredict{
    avghumidity: number;
    avgtemp_c : number;
    avgtemp_f: number;
    avgvis_km: number;
    avgvis_miles: number;
    condition: Condition;
    maxtemp_c: number;
    maxtemp_f: number;
    maxwind_kph : number;
    maxwind_mph: number;
    mintemp_c: number;
    mintemp_f: number;
    totalprecip_in: number;
    totalprecip_mm : number;
}