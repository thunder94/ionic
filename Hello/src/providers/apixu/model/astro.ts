export interface Astro {
    sunrise: string;
    sunset: string;
    moonrise: string;
    monset: string;
}