import {WeatherDetail, WeatherPredict} from "./weatherDetail";
import {Astro} from "./astro";

export interface ForecastDay {
    date: string;
    date_epoch: number;
    day: WeatherPredict;
    astro: Astro;
}