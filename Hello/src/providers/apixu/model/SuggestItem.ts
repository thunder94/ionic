import {Coords} from "./Coords";
import {HasName} from "./HasName";

export interface SuggestItem extends Coords,HasName{
    id : number;
    region: string;
    country: string;
    url?: string;
}