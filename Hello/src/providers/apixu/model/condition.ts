/*
  Generated class for the ApixuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export interface Condition {
    text: string;
    icon: string;
    code: number;
}