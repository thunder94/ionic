import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {APIResult} from "./model/APIResult";
import {SuggestItem} from "./model/SuggestItem";

@Injectable()
export class ApixuProvider {
    public secretKey = '7cf7a5274c6b4027b57164157180712';
    public apiURL = 'http://api.apixu.com/v1';
    public getWeatherUrl = `${this.apiURL}/current.json?key=${this.secretKey}&q=`
    public getSuggestUrl = `${this.apiURL}/search.json?key=${this.secretKey}&q=`
    public getForecastUrl = `${this.apiURL}/forecast.json?key=${this.secretKey}`

    //Pass US Zipcode, UK Postcode, Canada Postalcode, IP address, Latitude/Longitude (decimal degree) or city name
    public getWeather(query: string): Observable<APIResult> {
        let result = this
            .http
            .get<APIResult>(`${this.getWeatherUrl}${query}`);
        return result;
    }
    public getSuggest(query: string): Observable<SuggestItem[]>{
        let result = this
            .http
            .get<SuggestItem[]>(`${this.getSuggestUrl}${query}`);
        return result;
    }
    public getForecast(query: string, days: number): Observable<APIResult>{
        let result = this
            .http
            .get<APIResult>(`${this.getForecastUrl}&q=${query}&days=${days}`);
        return result;
    }
    constructor(public http: HttpClient) {
        console.log('Hello ApixuProvider Provider');
    }
}
