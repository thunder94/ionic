import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Geolocation, Geoposition} from '@ionic-native/geolocation';
import {from} from "rxjs/observable/from";
import {Observable} from "rxjs";

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {

    constructor(public http: HttpClient, public locationService: Geolocation) {
        console.log('Hello LocationProvider Provider');
    }

    public getLocation(): Observable<Coordinates> {
        let promise = this.locationService
            .getCurrentPosition()
            .then(res => {
                console.log(res);
                return res.coords;
            });
        return from(promise);
    }
}
