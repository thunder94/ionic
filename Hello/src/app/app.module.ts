import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {TabPage} from "../pages/tab/tab";
import {HomePage} from '../pages/home/home';
import {ForecastPage} from "../pages/forecast/forecast";
import {AboutPage} from "../pages/about/about";
import {ApixuProvider} from '../providers/apixu/apixu';
import {HttpModule} from "@angular/http";
import {Geolocation} from '@ionic-native/geolocation'
import {HttpClientModule, HttpClient} from "@angular/common/http";
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { LocationProvider } from '../providers/location/location';
import {CommonModule} from "@angular/common";
import {CityPage} from "../pages/city/city";

export function setTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,
        TabPage,
        HomePage,
        CityPage,
        ForecastPage,
        AboutPage
    ],
    imports: [
        BrowserModule,
        CommonModule,
        IonicModule.forRoot(MyApp),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: setTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        TabPage,
        HomePage,
        CityPage,
        ForecastPage,
        AboutPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Geolocation,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ApixuProvider,
    LocationProvider,
    ]
})
export class AppModule {
}
