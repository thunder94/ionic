import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApixuProvider} from "../../providers/apixu/apixu";
import moment from "moment";
import {Forecast} from "../../providers/apixu/model/forecast";
import {APIResult} from "../../providers/apixu/model/APIResult";
import {ForecastDay} from "../../providers/apixu/model/forecastDay";

/**
 * Generated class for the ForecastPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-forecast',
    templateUrl: 'forecast.html',
})
export class ForecastPage {
    dayOfWeeks: string[] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    forecastResult: APIResult;
    constructor(public navCtrl: NavController, public navParams: NavParams, public apixu: ApixuProvider) {
        this
            .getForecast("Ho Chi Minh")
            .subscribe(result => {
                console.log(result);
                this.forecastResult = result;
                if (this.forecastResult != null) {
                    this.forecastResult.forecast.forecastday.forEach(e => {
                        let condText = e.day.condition.text;
                        if(condText.includes("cloudy")) e.day.condition.text = "Cloudy";
                        if(condText.includes("rain")) e.day.condition.text = "Rain";
                        e["weekDay"] = this.getDaysOfWeeks(e.date);
                    });

                }
            });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ForecastPage');
    }
    private getDaysOfWeeks(date: string){
        return this.dayOfWeeks[moment(date).weekday()];
    }
    private getForecast(query: string) {
        return this.apixu
            .getForecast(query, 10);
    }

}
