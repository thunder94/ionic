import {Component} from '@angular/core';
import {AboutPage} from "../about/about";
import {ForecastPage} from "../forecast/forecast";
import {HomePage} from "../home/home";
import {SplashScreen} from '@ionic-native/splash-screen';
import {TranslateService} from "@ngx-translate/core";
import {CityPage} from "../city/city";

@Component({
    templateUrl: 'tab.html',
})
export class TabPage {

    homePage = HomePage;
    foreCastPage = ForecastPage;
    aboutPage = AboutPage;
    cityPage = CityPage
    constructor(private splashScreen: SplashScreen, translate: TranslateService) {
        this.splashScreen.show();
        this.splashScreen.hide();
    }

}
