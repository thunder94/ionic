import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController  } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  lang: any;
  constructor(public navCtrl: NavController, public translate: TranslateService, public toastCtrl: ToastController) {
    this.lang = 'en';
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }
  switchLanguage() {
    this.translate.use(this.lang);
    this.presentToast(this.lang);
  };
  presentToast(language) {
    var msg;
    if (language == 'en')
    {
      msg = 'Language has been changed succesfully!';
    }
    if (language == 'vi')
    {
      msg = 'Thay đổi ngôn ngữ thành công!';
    }
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  };
  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  };

}
