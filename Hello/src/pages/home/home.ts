import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

import {ApixuProvider} from "../../providers/apixu/apixu";
import {APIResult} from "../../providers/apixu/model/APIResult";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {debounceTime, map} from "rxjs/operators";
import {SuggestItem} from "../../providers/apixu/model/SuggestItem";
import {LocationProvider} from "../../providers/location/location";
import {Geoposition} from "@ionic-native/geolocation";
import {HasName} from "../../providers/apixu/model/HasName";
import {Coords} from "../../providers/apixu/model/Coords";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
    providers: [ApixuProvider]
})
export class HomePage {
    autoCompleteRequestEvent = new Subject();
    searchRequestEvent = new Subject<Coords | HasName>();
    @ViewChild('map') mapElement: ElementRef;
    public name;
    public result: APIResult;
    public currentLocation: Coordinates;
    public suggestItems: SuggestItem[];

    constructor(public navCtrl: NavController,
                public apixu: ApixuProvider,
                public location: LocationProvider,
                public translateService: TranslateService) {
        this.name = "";
        //auto complete
        this.autoCompleteRequestEvent
            .pipe(debounceTime(500))
            .subscribe((query: string) => {
                console.log("Fetching Auto Complete;")
                this.apixu
                    .getSuggest(query)
                    .subscribe(result => {
                        this.suggestItems = result;
                    })
            })
        //search
        this.searchRequestEvent
        //only search after 1s
            .pipe(debounceTime(500))
            .subscribe((item: SuggestItem) => {
                let query: string = "";
                if (item.name != null) query = item.name;
                if (item.lat != null && item.lon != null) query = `${Math.ceil(item.lat)} ${Math.ceil(item.lon)}`;
                console.log("Fetching;")
                this.apixu
                    .getWeather(query)
                    .subscribe(result => {
                        this.result = result;
                        // clear
                        this.suggestItems = [];
                    })
            })
        this
            .getLocation()
            .subscribe(coords => {
                this.currentLocation = coords;
                this.search({lat: coords.latitude, lon: coords.longitude})
                //test search
            });
    }

    getDate(datetime) {
        var result = datetime.slice(0, 10);
        return result;
    }

    getTime(datetime) {
        var result = datetime.slice(-5);
        return result;
    }

    getLocation(): Observable<Coordinates> {
        return this.location.getLocation();
    }

    search(item: HasName | Coords) {
        this.searchRequestEvent.next(item);
    }

    autoComplete($e) {
        let query: string = $e.target.value;
        if (query == null) query = "";
        if (query !== null && query.length > 0) {
            this.autoCompleteRequestEvent.next(query);
        } else {
            this.suggestItems = [];
        }
    }

}
